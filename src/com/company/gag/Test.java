package com.company.gag;


public class Test {

    static Buffer buffer = new Buffer(10);

    public static void main(String[] args) {

        Thread threadProducer = new Thread(new Producer());
        Thread threadConsumer = new Thread(new Consumer());

        threadProducer.start();
        threadConsumer.start();
    }
}
