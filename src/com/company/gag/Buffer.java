package com.company.gag;

public class Buffer {
    Integer[] arr;
    boolean flag = true;
    int count = -1;

    public Buffer(int size) {
        arr = new Integer[size];
    }

    public boolean add(Integer elem) {
        int arrLastElem = arr.length - 1;
        if (flag) {
            shift();
            arr[arrLastElem] = elem;
            ++count;
        } else {
            return false;
        }
        if (count == arrLastElem) {
            flag = false;
        }
        return true;
    }

    public Integer get() {
        if (!flag) {
            Integer elem = arr[0];
            arr[0] = null;
            shift();
            if (count == 0) {
                flag = true;
            }
            count--;
            return elem;
        }

        return null;
    }

    private void shift() {
        for (int i = 0; i < arr.length - 1; i++) {
            Integer temp = arr[i + 1];
            arr[i + 1] = arr[i];
            arr[i] = temp;
        }
    }

}
