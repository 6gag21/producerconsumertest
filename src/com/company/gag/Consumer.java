package com.company.gag;

class Consumer implements Runnable {

    @Override
    public void run() {
        while (true) {
            Integer elem = Test.buffer.get();
            if (elem != null) {
                System.out.println("get : " + elem);
            }
        }
    }
}
